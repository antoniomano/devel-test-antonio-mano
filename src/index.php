<?php
/*
 * author Antonio Mano
 * 
 */
include_once 'start.php';

$mensagem = "";
$titulo = "";
$autor = "";
$texto = "";
$identificador = "";
$operacao = "I"; 

if ($_SERVER['REQUEST_METHOD']=='POST') {
  if (isset($_POST['gravar'])) {
    $nova_pagina = new Pagina();
    $nova_pagina->set("title", $_POST['title']);
    $nova_pagina->set("slug", strtolower(str_replace("-", " ", $_POST['title'])));
    $nova_pagina->set("author", ucwords($_POST['author']));
    $nova_pagina->set("body", $_POST['body']);
    $nova_pagina->set("user_id", 1);
    
    if ($_POST['op']=="U") {
      $nova_pagina->set("id", $_POST['id']);
      if ($nova_pagina->update()) {
        $mensagem = "Pagina atualizada com sucesso!";
      } else {
        $mensagem = "Falha na atualizacao da pagina.";
      }    
    } else {
      if ($nova_pagina->insert()) {
        $mensagem = "Pagina cadastrada com sucesso!";
      } else {
        $mensagem = "Falha no cadastro da pagina.";
      }
    }    
  }

} else if (isset($_REQUEST['excluir'])) {
  $nova_pagina = new Pagina();
  $nova_pagina->set("id", $_REQUEST['id_del']);
  
  if ($nova_pagina->delete()) {
    $mensagem = "Pagina excluida com sucesso!";
  } else {
    $mensagem = "Falha na exclusao da pagina.";
  }    
} else if (isset($_REQUEST['alterar'])) {
  $nova_pagina = new Pagina();
  $nova_pagina->set("id", $_REQUEST['id_upd']);
  $dados_da_pagina = $nova_pagina->select();
  
  $titulo = $dados_da_pagina[0]['title'];
  $autor = $dados_da_pagina[0]['author'];
  $texto = $dados_da_pagina[0]['body'];
  $identificador = $_REQUEST['id_upd']; 
  $operacao = "U";    
}
?>

<style type="text/css">
  label {display: block;}
  tr {background-color: #ccc; color: #333;}
  div {width: 500px;}
  #formulario {width: 500px; background-color: #f4f4f4; padding: 5px; margin: 0 auto; border: solid 1px #ccc;}
</style>

<div>
  <div style="text-align: center;">
    <p>&nbsp;<?php echo $mensagem; ?>&nbsp;</p>
  </div>
  <div id="formulario">
    <form action="" method="POST">
      <input type="hidden" name="op" value="<?php echo $operacao;?>" />
      <input type="hidden" name="id" value="<?php echo $identificador;?>" />
      
      <label for="title">Titulo</label>
      <input type="text" name="title" style="width: 100%;" value="<?php echo $titulo;?>"/>
      
      <label for="author">Autor</label>
      <input type="text" name="author" style="width: 100%;" value="<?php echo $autor;?>"/>
  
      <label for="body">Texto</label>
      <textarea name="body" rows="6" style="width: 100%;" value="<?php echo $texto;?>"></textarea>
      
      <br />
      <br />
      <input type="submit" name="gravar" value="GRAVAR" />
    </form>
  </div>
  <br />
  <br />
  <div id="tabela">
    <table width="100%" cellspacing="3" border="1" >
      <tr>
        <th width="200">Titulo</th>
        <th width="160">Autor</th>
        <th width="80">Alterar</th>
        <th width="60">Excluir</th>
      </tr>
      <?php
        $pagina = new Pagina();
        $listagem_das_paginas = $pagina->select();
        foreach($listagem_das_paginas as $paginas) 
        {
          echo "<tr>";
            echo "<td>".$paginas['title']."</td>";
            echo "<td>".$paginas['author']."</td>";
            echo "<td width=\"20\" style=\"text-align: center;\"><a href=\"?id_upd=".$paginas['id']."&alterar=U\">Alterar</a></td>";
            echo "<td width=\"20\" style=\"text-align: center;\"><a href=\"?id_del=".$paginas['id']."&excluir=D\">X</a></td>";
          echo "</tr>";
        }
      ?>
    </table>
  </div>
</div>