<?php
/**
 * Arquivo autoload
 * 
 * Este arquivo possui a atribuicao de carregar as classes requisitadas
 * 
 * @copyright 2014 AntonioMano
 * @author Antonio Mano <antoniomano@msn.com>
 * @since 1.0      
 *  
 */

/**
 * Verifica a existencia da classe requisitada para fazer o include
 * 
 */  
function __autoload($classe) 
{
  if (file_exists("classes/".$classe.".class.php")) {
    include_once "classes/".$classe.".class.php";
  } else {
    echo "A Classe {".$classe."} nao existe.";  
  }
}