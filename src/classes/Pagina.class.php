<?php
/**
 * Classe Pagina
 * 
 * Esta classe implementa o CRUD na base de dados na tabela pagina
 * 
 * @copyright 2014 AntonioMano
 * @author Antonio Mano <antoniomano@msn.com>
 * @since 1.0      
 * @package classes 
 */

require_once 'Conexao.class.php';

class Pagina extends Conexao
{
  /**
   * Atributos que fazem referencia aos campos da tabela pagina
   */    
  private $id = null;
  private $title = null;
  private $slug = null;
  private $author = null;
  private $body = null;
  private $user_id = null;
  private $insert_date = null;
  private $update_date = null;  
  
  /**
   * Propriedade SET generica
   *  
   */      
  public function set($nome,$valor){
    $this->$nome = $valor;
  }
  
  /**
   * Propriedade GET generica
   *  
   */      
  public function get($nome){
    return $this->$nome;
  }
  
  /**
   * Realiza a insercao de dados na tabela pagina
   *  
   */    
  public function insert() 
  {
    $pdo = $this->conectar();
    try {
      $inserir = $pdo->prepare("INSERT INTO pagina (title, slug, author, body, user_id, insert_date) VALUES (:titulo, :url_amigavel, :autor, :texto, :usuario, NOW())");
      $inserir->bindValue(":titulo", $this->get("title"));
      $inserir->bindValue(":url_amigavel", $this->get("slug"));
      $inserir->bindValue(":autor", $this->get("author"));
      $inserir->bindValue(":texto", $this->get("body"));
      $inserir->bindValue(":usuario", $this->get("user_id"));
      $inserir->execute();
      
      if ($inserir->rowCount()==1) {
        return true;
      } else {
        return false;
      }
    } catch (PDOException $e) {
      echo "Falha na insercao da pagina no banco de dados: ".$e->getMessage();
    }
  }

  /**
   * Realiza a alteracao de dados na tabela pagina
   *  
   */    
  public function update() 
  {
    $pdo = $this->conectar();
    try {
      $atualizar = $pdo->prepare("UPDATE pagina SET title = :titulo, slug = :url_amigavel, author = :autor, body = :texto, update_date = NOW() WHERE id = :identificador");
      $atualizar->bindValue(":titulo", $this->get("title"));
      $atualizar->bindValue(":url_amigavel", $this->get("slug"));
      $atualizar->bindValue(":autor", $this->get("author"));
      $atualizar->bindValue(":texto", $this->get("body"));
      $atualizar->bindValue(":identificador", $this->get("id"));
      $atualizar->execute();

      if ($atualizar->rowCount()>0) {
        return true;
      } else {
        return false;
      }
    } catch (PDOException $e) {
      echo "Falha na atualizacao da pagina no banco de dados: ".$e->getMessage();
    }
  }

  /**
   * Realiza a exclusao de dados na tabela pagina
   *  
   */      
  public function delete() 
  {
    $pdo = $this->conectar();
    try {
      $excluir = $pdo->prepare("DELETE FROM pagina WHERE id = :identificador");
      $excluir->bindValue(":identificador", $this->get("id"));
      $excluir->execute();
      
      if ($excluir->rowCount()>0) {
        return true;
      } else {
        return false;
      }
    } catch (PDOException $e) {
      echo "Falha na insercao da pagina no banco de dados: ".$e->getMessage();
    }
  }

  /**
   * Realiza a listagem de dados na tabela pagina
   *  
   */      
  public function select() 
  {
    $pdo = $this->conectar();
    try {
      if ($this->get("id")) { 
        $listar = $pdo->prepare("SELECT * FROM pagina WHERE id = :identificador");
        $listar->bindValue(":identificador", $this->get("id"));
      } else {
        $listar = $pdo->prepare("SELECT * FROM pagina");
      }
      $listar->execute();
      
      if ($listar->rowCount()>0) {
        return $listar->fetchAll(PDO::FETCH_ASSOC);
      } else {
        return false;
      }
    } catch (PDOException $e) {
      echo "Falha ao listar as paginas no banco de dados: ".$e->getMessage();
    }
  }

}

