<?php
/**
 * Classe Conexao
 * 
 * Esta classe implementa a comunicacao com a base de dados
 * 
 * @copyright 2014 AntonioMano
 * @author Antonio Mano <antoniomano@msn.com>
 * @since 1.0      
 * @package classes 
 */

class Conexao
{
  /**
   * Este e o atributo que ira armazenar a conexao
   */
  private static $conexao;
  
  /**
   * Realiza a conexao com a base de dados
   * 
   */  
  public static function conectar() 
  {
    try {
      if (!isset(self::$conexao))
      {
        //$con = new PDO($dsn, $username, $passwd, $options);
        self::$conexao = new PDO("mysql:host=localhost:3310;dbname=tonliv_db", "root", "");
      }
    } catch (PDOException $e) {
      echo "Falha na conex�o com o banco de dados: ".$e->getMessage();
    }
    return self::$conexao;
  }
}

