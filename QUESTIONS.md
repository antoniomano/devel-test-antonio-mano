## Referências Técnicas

1. Quais foram os últimos dois livros técnicos que você leu?
R. Confesso que não estou muito ativo na leitura de livros técnicos. Os dois últimos livros que li, entenda-se folheei, foram o Livro Flex 4 Avançado e o Aprendendo a programar em JAVA2. 

2. Quais foram os últimos dois framework/CMS que você trabalhou?
R. Tenho experiência com os frameworks Flex e .Net. Não possuo experiência com frameworks PHP. Sei que isto não é uma boa notícia e até, parece ser incoerente com os anos de trabalho desenvolvendo em PHP, contudo, os trabalhos freelances que surgiram não demandaram a utilização de um framework, ou não possuíam tempo suficiente para a implementação de um. Ainda em PHP, já trabalhei com os CMSs Wordpress e Joomla.

3. Descreva os principais pontos positivos do seu framework favorito.
R. Acredito que esta questão, relaciona-se exclusivamente à frameworks PHP. Como informei na questão anterior (2). Não possuo uma opinião sólida sobre o assunto. 

4. Descreva os principais pontos negativos do seu framework favorito.
R. Acredito que esta questão, também relaciona-se exclusivamente à frameworks PHP. Como informei nas questões anteriores (2 e 3). Não possuo uma opinião sólida sobre o assunto. 

5. O que é código de qualidade para você.
R. Um código de qualidade deve ser eficaz e eficiente. Deve ser eficiente, principalmente, numa boa execução do que o mesmo foi idealizado à realizar, mas só será eficaz, se executar da melhor forma. Deve também ser simples e bem documentado, para que a manutenção do mesmo, possa ser feito por várias pessoas sem maiores esforços de interpretação.

## Conhecimento Linux

1. O que é software livre?
R. Basicamente, é um software cujo o código fonte esteja disponível, com ou sem restrições, para a comunidade (leia-se qualquer um) usar, copiar, estudar, modificar e até, redistribuir.

2. Qual o seu sistema operacional favorito?
R. Mesmo que esta resposta me desclassifique, tenho que ser honesto: Sou um usuário Windows. O custo elevado dos produtos da “maçã” não me permitiram me tornar um usuário Mac e os trabalhos em VB.Net não me permitiram ser um usuário, exclusivamente, Linux. Utilizei o Ubuntu, de forma (quase) exclusiva, durante todo o ano de 2010 e posso dizer que foi uma boa experiência.

3. Já trabalhou com Linux ou outro Unix-like?
R. Sim. Em 2010, o único sistema operacional principal do meu notebook era o Ubuntu.

4. O que é SSH?
R. Não tenho conhecimento sobre o tema, apenas que é um protocolo seguro de comunicação e etc.

5. Quais as principais diferenças entre sistemas *nix e o Windows?
R. O Windows é um software proprietário. Penso que todas as demais distinções resvalam nesta questão única.

## Conhecimento de desenvolvimento

1. O que é GIT?
R. Basicamente, é um Sistema de Controle de Versões (Muito poderoso pelo que vi). Confesso que estou utilizando-o pela primeira vez, para realizar este teste e estou gostando muito.

2. Descreva um workflow simples de trabalho utilizando GIT.
R. Como eu informei na questão anterior, não havia trabalhado com GIT até o momento, contudo, penso que um bom fluxo seria: Criar um Branch do projeto => "commitar" as implementações/alterações => realizar um pull request das mudanças "commitadas" => discutir/rever/aprovar as mudanças => executar um merge das mudanças com o projeto.

3. O que é PHP Data Objects?
R. PDO é um conjuto de classes para comunicação com a base de dados. Confesso que (infelizmente) nunca utilizei PDO, pois sempre fiz uso da biblioteca de funções do MySQL.

4. O que é Database Abstract Layer?
R. Não sei. (É óbvio que, agora, eu pesquisei na internet e tomei noção do que se trata, mas preferi responder com sinceridade)

5. Você sabe o que é Object Relational Mapping? Se sim, explique.
R. Não sei. (É óbvio que, agora, eu pesquisei na internet e tomei noção do que se trata, mas preferi responder com sinceridade)

6. Como você avalia seu grau de conhecimento em Orientação a objeto?
R. Minha noção teórica é intermediária, contudo, não a apliquei plenamente em meus últimos projetos. Acredito que em pouco tempo de prática, eu esteja num nível avançado.

7. O que é Dependency Injection?
R. Não sei. (É óbvio que, agora, eu pesquisei na internet e tomei noção do que se trata, mas preferi responder com sinceridade)

8. O significa a sigla S.O.L.I.D?
R. Não sei. (É óbvio que, agora, eu pesquisei na internet e tomei noção do que se trata, mas preferi responder com sinceridade)

9. Qual a finalidade do framework PHPUnit?
R. É um framework de testes. Confesso, infelizmente, que nunca o utilizei.

10. Explique o que é MVC.
R.  MVC é um modelo de arquitetura de software. Confesso, infelizmente, que não tenho utilizado MVC em meus projetos.